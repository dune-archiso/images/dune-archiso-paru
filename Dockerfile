# Copyleft (c) December, 2021, Oromion.
# Usage: docker build -t dune-archiso/dune-archiso:dune-core .

FROM registry.gitlab.com/dune-archiso/images/dune-archiso

LABEL maintainer="Oromion <caznaranl@uni.pe>" \
  name="Dune paru" \
  description="Dune in paru" \
  url="https://gitlab.com/dune-archiso/images/dune-archiso-paru/container_registry" \
  vcs-url="https://gitlab.com/dune-archiso/images/dune-archiso-paru" \
  vendor="Oromion Aznarán" \
  version="1.0"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ARG AUR_PARU="https://aur.archlinux.org/cgit/aur.git/snapshot/paru.tar.gz"

RUN sudo pacman --needed --noconfirm --noprogressbar -Syyuq && \
  mkdir -p ~/build && \
  cd ~/build && \
  curl -LO ${AUR_PARU} && \
  tar -xvf paru.tar.gz && \
  cd paru && \
  makepkg --noconfirm -si && \
  rm -rf ~/build ~/.cargo /tmp/makepkg && \
  pacman -Qtdq | xargs -r sudo pacman --noconfirm -Rcns && \
  sudo pacman -Scc <<< Y <<< Y && \
  sudo rm -r /var/lib/pacman/sync/*